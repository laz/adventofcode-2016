package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	registers := map[string]int{"a": 7, "b": 0, "c": 0, "d": 0}
	commands := make([][]string, 0, 25)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), " ")
		commands = append(commands, parts)
	}

	toggle := map[string]string{
		"inc": "dec",
		"dec": "inc",
		"tgl": "inc",
		"jnz": "cpy",
		"cpy": "jnz",
	}

	for i := 0; i < len(commands); {
		//fmt.Println(i, strings.Join(commands[i], " "), registers)
		line := commands[i]
		command := line[0]
		args := line[1:]

		switch command {
		case "cpy":
			if val, ok := registers[args[0]]; ok {
				if _, ok = registers[args[1]]; ok {
					registers[args[1]] = val
				}
			} else if val, err := strconv.Atoi(args[0]); err == nil {
				if _, ok = registers[args[1]]; ok {
					registers[args[1]] = val
				}
			}
			i++
		case "inc":
			val, ok := registers[args[0]]
			if ok {
				registers[args[0]] = val + 1
			}
			i++
		case "dec":
			val, ok := registers[args[0]]
			if ok {
				registers[args[0]] = val - 1
			}
			i++
		case "jnz":
			move := 1
			parseit := false

			val, ok := registers[args[0]]
			if ok {
				parseit = val != 0
			} else {
				parseit = args[0] != "0"
			}

			if parseit {
				if d, ok := registers[args[1]]; ok {
					move = d
				} else if d, err := strconv.Atoi(args[1]); err == nil {
					move = d
				}
			}
			i += move
		case "tgl":
			target := i
			val, ok := registers[args[0]]
			if ok {
				target = i + val
			}

			if target < len(commands) {
				commands[target][0] = toggle[commands[target][0]]
				//fmt.Println(i, strings.Join(commands[i], " "), registers, "toggled", strings.Join(commands[target], " "), "at", target)
			}
			i++
		case "out":
			b, _ := strconv.Atoi(args[0])
			fmt.Print(string(b))
			i++
		}
	}

	fmt.Println(registers["a"])
}
