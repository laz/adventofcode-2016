package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	registers := map[string]int{"a": 0, "b": 0, "c": 0, "d": 0}
	commands := make([][]string, 0, 25)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), " ")
		commands = append(commands, parts)
	}

	opt := optimize(commands)

	toggle := map[string]string{
		"inc": "dec",
		"dec": "inc",
		"tgl": "inc",
		"jnz": "cpy",
		"cpy": "jnz",
	}

	outs := &bytes.Buffer{}
	target := "010101010101"
	currentA := 0

	for outs.String() != target {
		currentA++
		registers["a"] = currentA
		registers["b"] = 0
		registers["c"] = 0
		registers["d"] = 0
		outs.Reset()

		for i := 0; i < len(commands); {
			line := commands[i]
			if len(opt[i]) > 0 {
				line = opt[i]
			}
			command := line[0]
			args := line[1:]

			switch command {
			case "cpy":
				if val, ok := registers[args[0]]; ok {
					if _, ok = registers[args[1]]; ok {
						registers[args[1]] = val
					}
				} else if val, err := strconv.Atoi(args[0]); err == nil {
					if _, ok = registers[args[1]]; ok {
						registers[args[1]] = val
					}
				}
				i++
			case "inc":
				val, ok := registers[args[0]]
				if ok {
					registers[args[0]] = val + 1
				}
				i++
			case "dec":
				val, ok := registers[args[0]]
				if ok {
					registers[args[0]] = val - 1
				}
				i++
			case "jnz":
				move := 1
				parseit := false

				val, ok := registers[args[0]]
				if ok {
					parseit = val != 0
				} else {
					parseit = args[0] != "0"
				}

				if parseit {
					if d, ok := registers[args[1]]; ok {
						move = d
					} else if d, err := strconv.Atoi(args[1]); err == nil {
						move = d
					}
				}
				i += move
			case "tgl":
				target := i
				val, ok := registers[args[0]]
				if ok {
					target = i + val
				}

				if target < len(commands) {
					commands[target][0] = toggle[commands[target][0]]
					opt = optimize(commands)
				}
				i++
			case "out":
				val, ok := registers[args[0]]
				if !ok {
					val, _ = strconv.Atoi(args[0])
				}
				outs.WriteString(strconv.Itoa(val))
				i++
			case "add":
				_, ok := registers[args[0]]
				if ok {
					val, ok := registers[args[1]]
					if !ok {
						val, _ = strconv.Atoi(args[1])
					}
					registers[args[0]] += val
				}
				i++
			case "mul":
				_, ok := registers[args[0]]
				if ok {
					val, ok := registers[args[1]]
					if !ok {
						val, _ = strconv.Atoi(args[1])
					}
					registers[args[0]] *= val
				}
				i++
			}
			if outs.Len() == 12 {
				break
			}
		}
	}

	fmt.Println(currentA)
}

func optimize(commands [][]string) [][]string {
	optimized := make([][]string, len(commands))

	noop := []string{"jnz", "0", "0"}
	pattern := []string{"cpy", "inc", "dec", "jnz", "dec", "jnz"}
	args := []string{"r0 r1", "r2", "r1", "r1 -2", "r3", "r3 -5"}
	opt := [][]string{{"mul", "r3", "r0"}, {"add", "r2", "r3"}, {"cpy", "0", "r1"}, {"cpy", "0", "r3"}, noop, noop}

	regs := make([]string, 4)
	blank := make([]string, 4)

	for i := 0; i < len(commands); i++ {
		found := 0
		for j := 0; j < len(pattern) && found == j; j++ {
			if i+j >= len(commands) || commands[i+j][0] != pattern[j] {
				copy(regs, blank)
				break
			} else {
				c := commands[i+j]
				parts := strings.Split(args[j], " ")
				k := parts[0][1] - '0'

				if regs[k] == "" || regs[k] == c[1] {
					regs[k] = c[1]
					if len(parts) == 2 {
						if parts[1][0] != 'r' {
							if c[2] == parts[1] {
								found++
							}
						} else {
							k := parts[1][1] - '0'
							regs[k] = c[2]
							found++
						}
					} else {
						found++
					}
				}
			}
		}
		if found == len(pattern) {
			for j, o := range opt {
				t := make([]string, len(o))
				copy(t, o)
				if len(t) > 1 {
					if t[1][0] == 'r' {
						t[1] = regs[t[1][1]-'0']
					}
				}
				if len(t) > 2 {
					if t[2][0] == 'r' {
						t[2] = regs[t[2][1]-'0']
					}
				}
				optimized[i+j] = t
			}
		}
	}

	return optimized
}
