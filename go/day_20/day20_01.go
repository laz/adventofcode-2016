package main

import (
	"bufio"
	"fmt"
	"os"
)

type ipRange [2]uint32

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	answer := uint32(0)
	ranges := make([]ipRange, 0, 1000)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		input := scanner.Text()
		low, high := uint32(0), uint32(0)
		fmt.Sscanf(input, "%d-%d", &low, &high)
		ranges = append(ranges, ipRange{low, high})

		if answer >= low && answer <= high {
			answer = high + 1
		}
	}

	finished := false
	for !finished {
		finished = true
		deleteIndexes := []int{}
		for i := 0; i < len(ranges); i++ {
			r := ranges[i]
			if answer >= r[0] && answer <= r[1] {
				answer = r[1] + 1
			} else if answer > r[1] {
				deleteIndexes = append(deleteIndexes, i)
				finished = false
			}
		}
		for i := len(deleteIndexes) - 1; i >= 0; i-- {
			d := deleteIndexes[i]
			ranges = append(ranges[:d], ranges[d+1:]...)
		}
	}

	fmt.Println(answer)
}
