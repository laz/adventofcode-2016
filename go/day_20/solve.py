def get_range(line):
    return map(int, line.split('-'))

def solve(inp):
    ranges = sorted([get_range(line) for line in inp])
    mn, mx = ranges[0]
    tot = 0
    for r in ranges:
        if r[0] > mx+1:
            tot += mx-mn+1
            mn = r[0]
            mx = r[1]
        else:
            mx = max(mx, r[1])
    return 4294967296 - tot - (mx-mn+1)

fname = 'input'
inp = [line.strip() for line in open(fname).readlines()]
print solve(inp)
