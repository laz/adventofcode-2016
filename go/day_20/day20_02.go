package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
)

type ipRange [2]uint32
type ipRangeList []ipRange

func (l ipRangeList) Len() int           { return len(l) }
func (l ipRangeList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l ipRangeList) Less(i, j int) bool { return l[i][0] < l[j][0] }

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	answer := uint32(0)
	ranges := ipRangeList(make([]ipRange, 0, 1000))
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		input := scanner.Text()
		low, high := uint32(0), uint32(0)
		fmt.Sscanf(input, "%d-%d", &low, &high)
		ranges = append(ranges, ipRange{low, high})

		if answer >= low && answer <= high {
			answer = high + 1
		}
	}

	sort.Sort(ranges)

	consolidated := false
	for !consolidated {
		deleteIndexes := []int{}
		consolidated = true
		for i := 1; i < len(ranges); i++ {
			prev := &ranges[i-1]
			curr := &ranges[i]
			del := false
			if curr[1] <= prev[1] {
				del = true
			} else if curr[0] <= prev[1]+1 {
				del = true
				prev[1] = curr[1]
			}
			if del {
				deleteIndexes = append(deleteIndexes, i)
				consolidated = false
				i++
			}
		}
		for i := len(deleteIndexes) - 1; i >= 0; i-- {
			d := deleteIndexes[i]
			ranges = append(ranges[:d], ranges[d+1:]...)
		}
	}

	blocked := uint32(0)
	for i := 0; i < len(ranges); i++ {
		curr := &ranges[i]
		blocked += (curr[1] - curr[0]) + 1
	}

	fmt.Println((math.MaxUint32 - blocked) + 1)
}
