package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const safe = '.'
const trap = '^'

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}
	scanner := bufio.NewScanner(r)

	rows := 40
	if len(os.Args) > 2 {
		rows, _ = strconv.Atoi(os.Args[2])
	}

	scanner.Scan()
	row := string(safe) + scanner.Text() + string(safe)

	safeTiles := 0
	t := string(trap)
	s := string(safe)
	for i := 0; i < rows; i++ {
		safeTiles += strings.Count(row, s) - 2
		next := make([]byte, len(row))
		next[0] = safe
		next[len(next)-1] = safe
		for j := 1; j < len(next)-1; j++ {
			c := strings.Count(row[j-1:j+2], t)
			if (row[j] == safe && c == 1) ||
				(row[j] == trap && c == 2) {
				next[j] = trap
			} else {
				next[j] = safe
			}
		}
		row = string(next)
	}

	fmt.Println(safeTiles)
}
