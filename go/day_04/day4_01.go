package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

type count struct {
	c rune
	n int
}

type countOrder []*count

func (o countOrder) Len() int      { return len(o) }
func (o countOrder) Swap(i, j int) { o[i], o[j] = o[j], o[i] }
func (o countOrder) Less(i, j int) bool {
	if o[i].n == o[j].n {
		return o[i].c < o[j].c
	}
	return o[i].n > o[j].n
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	sum := 0
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), "-")
		idsum := strings.Split(parts[len(parts)-1], "[")
		id, _ := strconv.Atoi(idsum[0])
		chk := strings.TrimSuffix(idsum[1], "]")

		room := strings.Join(parts[:len(parts)-1], "")
		m := map[rune]*count{}
		for _, ch := range room {
			co, ok := m[ch]
			if !ok {
				co = &count{ch, 0}
				m[ch] = co
			}
			co.n += 1
		}
		vals := countOrder(make([]*count, 0, len(m)))
		for _, v := range m {
			vals = append(vals, v)
		}

		sort.Sort(vals)
		actual := ""
		for _, v := range vals[:5] {
			actual += string(v.c)
		}
		if actual == chk {
			sum += id
		}
	}
	fmt.Println(sum)
}
