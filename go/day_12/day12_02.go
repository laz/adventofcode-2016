package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	registers := map[string]int{"a": 0, "b": 0, "c": 1, "d": 0}
	commands := make([][]string, 0, 25)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), " ")
		commands = append(commands, parts)
	}

	for i := 0; i < len(commands); {
		line := commands[i]
		command := line[0]
		args := line[1:]

		switch command {
		case "cpy":
			if val, ok := registers[args[0]]; ok {
				registers[args[1]] = val
			} else if val, err := strconv.Atoi(args[0]); err == nil {
				registers[args[1]] = val
			}
			i++
		case "inc":
			val, ok := registers[args[0]]
			if ok {
				registers[args[0]] = val + 1
			}
			i++
		case "dec":
			val, ok := registers[args[0]]
			if ok {
				registers[args[0]] = val - 1
			}
			i++
		case "jnz":
			move := 1
			parseit := false

			val, ok := registers[args[0]]
			if ok {
				parseit = val != 0
			} else {
				parseit = args[0] != "0"
			}

			if parseit {
				if d, err := strconv.Atoi(args[1]); err == nil {
					move = d
				}
			}
			i += move
		case "out":
			b, _ := strconv.Atoi(args[0])
			fmt.Print(string(b))
			i++
		}
	}

	fmt.Println(registers["a"])
}
