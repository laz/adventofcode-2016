package main

import (
	"bufio"
	"crypto/md5"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	scanner.Scan()
	input := scanner.Text()

	r.Close()

	size := 8
	code := make([]byte, size)
	num := 0
	cur := 0
	for num < size {
		sum := ""
		for !strings.HasPrefix(sum, "00000") {
			test := input + strconv.Itoa(cur)
			sum = fmt.Sprintf("%x", md5.Sum([]byte(test)))
			cur++
		}
		pos, err := strconv.Atoi(string(sum[5]))
		if err == nil && pos < size && code[pos] == 0 {
			code[pos] = sum[6]
			num++
		}
	}
	fmt.Println(string(code))
}
