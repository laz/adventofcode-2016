package main

import (
	"bufio"
	"crypto/md5"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	scanner.Scan()
	input := scanner.Text()

	r.Close()

	code := make([]byte, 8)
	cur := 0
	for i := 0; i < len(code); i++ {
		sum := ""
		for !strings.HasPrefix(sum, "00000") {
			test := input + strconv.Itoa(cur)
			sum = fmt.Sprintf("%x", md5.Sum([]byte(test)))
			cur++
		}
		code[i] = sum[5]
	}
	fmt.Println(string(code))
}
