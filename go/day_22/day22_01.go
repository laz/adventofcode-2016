package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
)

type node struct {
	x     int
	y     int
	total int
	used  int
	free  int
	per   int
}

type usedNodeList []node

func (n usedNodeList) Len() int           { return len(n) }
func (n usedNodeList) Swap(i, j int)      { n[i], n[j] = n[j], n[i] }
func (n usedNodeList) Less(i, j int) bool { return n[i].used < n[j].used }

type freeNodeList []node

func (n freeNodeList) Len() int           { return len(n) }
func (n freeNodeList) Swap(i, j int)      { n[i], n[j] = n[j], n[i] }
func (n freeNodeList) Less(i, j int) bool { return n[i].free < n[j].free }

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	used := usedNodeList(make([]node, 0, 1000))
	free := freeNodeList(make([]node, 0, 1000))
	parser := regexp.MustCompile(`/dev/grid/node-x(\d+?)-y(\d+?)\s+(\d+?)T\s+(\d+?)T\s+(\d+?)T\s+(\d+?)%`)
	for scanner.Scan() {
		m := parser.FindStringSubmatch(scanner.Text())
		if len(m) > 0 {
			n := make([]int, len(m))
			for i, s := range m[1:] {
				n[i], _ = strconv.Atoi(s)
			}
			nd := node{n[0], n[1], n[2], n[3], n[4], n[5]}
			used = append(used, nd)
			free = append(free, nd)
		}
	}

	size := len(used)
	sort.Sort(used)
	sort.Sort(sort.Reverse(free))

	seen := map[string]bool{}
	pairs := 0
	for f := 0; f < size; f++ {
		for u := 0; u < size; u++ {
			if visited(seen, free[f], used[u]) ||
				(free[f].x == used[u].x && free[f].y == used[u].y) {
				continue
			}
			if free[f].free >= used[u].used && used[u].used > 0 {
				visit(seen, free[f], used[u])
				pairs++
			} else {
				break
			}
		}
	}

	fmt.Println(pairs)
}

func visited(m map[string]bool, n1, n2 node) bool {
	k1, k2 := keys(n1, n2)
	if m[k1] {
		return true
	}
	if m[k2] {
		return true
	}
	return false
}

func visit(m map[string]bool, n1, n2 node) {
	k1, k2 := keys(n1, n2)
	m[k1] = true
	m[k2] = true
}

func keys(n1, n2 node) (string, string) {
	k1 := fmt.Sprintf("%d:%d-%d:%d", n1.x, n1.y, n2.x, n2.y)
	k2 := fmt.Sprintf("%d:%d-%d:%d", n2.x, n2.y, n1.x, n1.y)
	return k1, k2
}
