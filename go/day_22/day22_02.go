package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

type node struct {
	x     int
	y     int
	total int
	used  int
	free  int
	per   int
}

type grid [][]int

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	maxFree := node{}
	mx, my := 0, 0
	nodes := make([]node, 0, 1000)
	parser := regexp.MustCompile(`/dev/grid/node-x(\d+?)-y(\d+?)\s+(\d+?)T\s+(\d+?)T\s+(\d+?)T\s+(\d+?)%`)
	for i := 0; scanner.Scan(); i++ {
		m := parser.FindStringSubmatch(scanner.Text())
		if len(m) > 0 {
			n := make([]int, len(m))
			for i, s := range m[1:] {
				n[i], _ = strconv.Atoi(s)
			}
			nd := node{n[0], n[1], n[2], n[3], n[4], n[5]}
			if nd.x > mx {
				mx = nd.x
			}
			if nd.y > my {
				my = nd.y
			}
			nodes = append(nodes, nd)
			if nd.used == 0 && nd.free > maxFree.free {
				maxFree = nd
			}
		}
	}

	my++
	mx++

	initialGrid := grid(make([][]int, my))
	for y := 0; y < my; y++ {
		initialGrid[y] = make([]int, mx)
	}

	for i, n := range nodes {
		initialGrid[n.y][n.x] = i
	}

	fmt.Println(mx, "x", my)

	fmt.Println()
	for _, row := range initialGrid {
		for _, n := range row {
			nd := nodes[n]
			if nd == maxFree {
				fmt.Print("_ ")
			} else if nd.used > maxFree.free {
				fmt.Print("# ")
			} else if nd.x == mx-1 && nd.y == 0 {
				fmt.Print("G ")
			} else {
				fmt.Print(". ")
			}
		}
		fmt.Println()
	}
	fmt.Println()

	/*
		targetIndex := initialGrid[0][mx]
		startingNode := nodes[0]
		moves := 0
		seen := map[grid]bool{}

		domain := []grid{initialGrid}
		for len(domain) > 0 {
			tests := []grid{}
			for _, currentGrid := range domain {
				if currentGrid[0][0] == targetIndex {
					domain := []grid{}
					break
				}
			}
			domain = next
			moves++
		}
	*/
}
