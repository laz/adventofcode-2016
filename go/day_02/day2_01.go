package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type point struct {
	x int
	y int
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	location := point{1, 1}
	move := map[byte]point{'L': point{-1, 0}, 'R': point{1, 0}, 'U': point{0, -1}, 'D': point{0, 1}}
	code := ""

	adjust := func(i int) int {
		if i < 0 {
			return 0
		}
		if i > 2 {
			return 2
		}
		return i
	}
	input := bufio.NewReader(r)

	for {
		b, err := input.ReadByte()
		if err != nil {
			break
		}
		d, ok := move[b]
		if ok {
			location.x = adjust(location.x + d.x)
			location.y = adjust(location.y + d.y)
		} else {
			code = code + strconv.Itoa((location.x+1)+(location.y*3))
		}
	}
	fmt.Println(code)
}
