package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type point struct {
	x int
	y int
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	keypad := [][]int{
		{0x0, 0x0, 0x1, 0x0, 0x0},
		{0x0, 0x2, 0x3, 0x4, 0x0},
		{0x5, 0x6, 0x7, 0x8, 0x9},
		{0x0, 0xa, 0xb, 0xc, 0x0},
		{0x0, 0x0, 0xd, 0x0, 0x0},
	}

	location := point{0, 2}
	moves := map[byte]point{'L': point{-1, 0}, 'R': point{1, 0}, 'U': point{0, -1}, 'D': point{0, 1}}
	code := ""

	move := func(p point, m point) point {
		r := p
		if (p.y+m.y) >= 0 && len(keypad) > (p.y+m.y) && len(keypad[p.y+m.y]) > p.x && keypad[p.y+m.y][p.x] != 0 {
			r.y = p.y + m.y
		}
		if (p.x+m.x) >= 0 && len(keypad) > p.y && len(keypad[p.y]) > (p.x+m.x) && keypad[p.y][p.x+m.x] != 0 {
			r.x = p.x + m.x
		}
		return r
	}

	input := bufio.NewReader(r)

	for {
		b, err := input.ReadByte()
		if err != nil {
			break
		}
		d, ok := moves[b]
		if ok {
			location = move(location, d)
		} else {
			code = code + strconv.FormatInt(int64(keypad[location.y][location.x]), 16)
		}
	}
	fmt.Println(code)
}
