package main

import (
	"crypto/md5"
	"fmt"
	"os"
)

type pair [2]int
type path struct {
	p pair
	s string
}

const (
	up = iota
	down
	left
	right
)

const side = 4

var moves = []pair{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}
var directions = []string{"U", "D", "L", "R"}

func main() {
	passcode := "hhhxzeay"
	if len(os.Args) > 1 {
		passcode = os.Args[1]
	}

	end := pair{3, 3}
	start := pair{0, 0}
	domain := []path{{p: start}}
	solution := path{}

	for 0 < len(domain) {
		next := []path{}
		for _, current := range domain {
			if current.p == end {
				solution = current
				continue
			}

			state := fmt.Sprintf("%x", md5.Sum([]byte(passcode+current.s)))
			for d := up; d <= right; d++ {
				if state[d] > 'a' {
					test, ok := move(current.p, d)
					if ok {
						s := current.s + directions[d]
						next = append(next, path{p: test, s: s})
					}
				}
			}
			domain = next
		}
	}
	fmt.Println(len(solution.s))
}

func move(p pair, d int) (pair, bool) {
	r := pair{
		p[0] + moves[d][0],
		p[1] + moves[d][1],
	}
	ok := r[0] >= 0 && r[1] >= 0 && r[0] < side && r[1] < side
	return r, ok
}
