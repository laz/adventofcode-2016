package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	inhyper := false
	hyperstart := '['
	hyperend := ']'
	size := 4
	sum := 0

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		test := scanner.Text()

		match := false
		for i, r := range test {
			if r == hyperstart {
				inhyper = true
			}
			if i+size <= len(test) {
				found := true
				if test[i] == test[i+1] {
					found = false
				}
				if found {
					j := i + size - 1
					for k := 0; k < size/2; k++ {
						if test[i+k] != test[j-k] {
							found = false
							break
						}
					}
				}
				if found {
					match = !inhyper
					if inhyper {
						break
					}
				}
				if r == hyperend {
					inhyper = false
				}
			}
		}
		if match {
			sum++
		}
	}
	fmt.Println(sum)
}
