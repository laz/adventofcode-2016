package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	inhyper := false
	hyperstart := '['
	hyperend := ']'
	sum := 0

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		test := scanner.Text()

		supermatches := make([]string, 0, len(test))
		hypermatches := map[string]struct{}{}
		for i, r := range test {
			if r == hyperstart {
				inhyper = true
			}
			if i+3 <= len(test) {
				found := true
				if test[i] == test[i+1] || rune(test[i+1]) == hyperstart || rune(test[i+1]) == hyperend {
					found = false
				}
				if found {
					j := i + 2
					if test[i] != test[j] {
						found = false
					}
				}
				if found {
					if !inhyper {
						supermatches = append(supermatches, test[i:i+3])
					} else {
						key := string([]byte{test[i+1], test[i], test[i+1]})
						hypermatches[key] = struct{}{}
					}
				}
				if r == hyperend {
					inhyper = false
				}
			}
		}
		match := false
		for _, s := range supermatches {
			_, match = hypermatches[s]
			if match {
				break
			}
		}
		if match {
			sum++
		}
	}
	fmt.Println(sum)
}
