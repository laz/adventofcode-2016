package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	possible := 0
	count := 0
	sides := []int{0, 0, 0}

	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		side, _ := strconv.Atoi(scanner.Text())
		sides[count%3] = side
		count++
		if count%3 == 0 {
			if sides[0]+sides[1] > sides[2] &&
				sides[0]+sides[2] > sides[1] &&
				sides[1]+sides[2] > sides[0] {
				possible++
			}
		}
	}

	fmt.Println(possible)
}
