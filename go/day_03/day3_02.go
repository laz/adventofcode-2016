package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	possible := 0
	count := 0
	tris := [][]int{
		{0, 0, 0},
		{0, 0, 0},
		{0, 0, 0},
	}

	test := func(sides []int) bool {
		return sides[0]+sides[1] > sides[2] &&
			sides[0]+sides[2] > sides[1] &&
			sides[1]+sides[2] > sides[0]
	}

	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		side, _ := strconv.Atoi(scanner.Text())
		tris[0][count%3] = side
		scanner.Scan()
		side, _ = strconv.Atoi(scanner.Text())
		tris[1][count%3] = side
		scanner.Scan()
		side, _ = strconv.Atoi(scanner.Text())
		tris[2][count%3] = side
		count++

		if count%3 == 0 {
			for _, tri := range tris {
				if test(tri) {
					possible++
				}
			}
		}
	}

	fmt.Println(possible)
}
