package main

import (
	"fmt"
	"os"
	"strconv"
)

type elf [2]int

func main() {
	size := 3014603
	if len(os.Args) > 1 {
		size, _ = strconv.Atoi(os.Args[1])
	}
	elves := make([]elf, size)

	for i := 0; i < size; i++ {
		elves[i] = elf{i + 1, 1}
	}

	for len(elves) > 1 {
		for i := 0; i < len(elves); i++ {
			if elves[i][1] > 0 {
				left := (i + 1) % len(elves)

				elves[i][1] += elves[left][1]
				elves[left][1] = 0
			}
		}
		pruned := make([]elf, 0, len(elves))
		for _, elf := range elves {
			if elf[1] > 0 {
				pruned = append(pruned, elf)
			}
		}
		elves = pruned
	}

	fmt.Println(elves)
}
