package main

import (
	"fmt"
	"os"
	"strconv"
)

type elf struct {
	id   int
	prev *elf
	next *elf
}

func (e *elf) drop() {
	e.prev.next = e.next
	e.next.prev = e.prev
}

func main() {
	size := 3014603
	if len(os.Args) > 1 {
		size, _ = strconv.Atoi(os.Args[1])
	}
	elves := make([]*elf, size)

	for i := 0; i < size; i++ {
		elves[i] = &elf{id: i + 1}
	}

	for i := 0; i < size; i++ {
		elves[i].prev = elves[(i+size-1)%size]
		elves[i].next = elves[(i+size+1)%size]
	}

	current := elves[0]
	mid := elves[size/2]

	for i := 0; i < size-1; i++ {
		mid.drop()
		mid = mid.next
		if (size-i)%2 == 1 {
			mid = mid.next
		}
		current = current.next
	}

	fmt.Println(current.id)
}
