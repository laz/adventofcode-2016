package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

type count struct {
	c rune
	n int
}

type countOrder []*count

func (o countOrder) Len() int      { return len(o) }
func (o countOrder) Swap(i, j int) { o[i], o[j] = o[j], o[i] }
func (o countOrder) Less(i, j int) bool {
	if o[i].n == o[j].n {
		return o[i].c < o[j].c
	}
	return o[i].n < o[j].n
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	more := scanner.Scan()
	message := scanner.Text()

	freq := make([]map[rune]*count, len(message))
	for i := 0; i < len(freq); i++ {
		freq[i] = map[rune]*count{}
	}

	for more {
		for i, ch := range message {
			m := freq[i]
			co, ok := m[ch]
			if !ok {
				co = &count{ch, 0}
				m[ch] = co
			}
			co.n += 1
		}
		more = scanner.Scan()
		if more {
			message = scanner.Text()
		}
	}

	result := ""
	for _, m := range freq {
		vals := countOrder(make([]*count, 0, len(m)))
		for _, v := range m {
			vals = append(vals, v)
		}

		sort.Sort(vals)
		result += string(vals[0].c)
	}
	fmt.Println(result)
}
