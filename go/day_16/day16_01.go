package main

import (
	"fmt"
	"os"
	"strconv"
)

var swap = map[byte]byte{'0': '1', '1': '0'}

func main() {
	size := 272
	if len(os.Args) > 1 {
		size, _ = strconv.Atoi(os.Args[1])
	}

	wipe := "10010000000110000"
	if len(os.Args) > 2 {
		wipe = os.Args[2]
	}

	l := len(wipe)
	for l < size {
		more := make([]byte, l)
		for i := 0; i < l; i++ {
			more[i] = swap[wipe[l-1-i]]
		}
		wipe += "0" + string(more)
		l = len(wipe)
	}

	if len(wipe) > size {
		wipe = wipe[:size]
	}

	sum := []byte(wipe)
	for len(sum)&1 == 0 {
		for i := 0; i < len(sum); i += 2 {
			sum[i/2] = checksum(sum[i], sum[i+1])
		}
		sum = sum[:len(sum)/2]
	}

	fmt.Println(string(sum))
}

func checksum(a byte, b byte) byte {
	if a == b {
		return '1'
	}
	return '0'
}
