package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const scanVal = "value %d goes to bot %d"
const scanBot = "bot %d gives low to %s %d and high to %s %d"

type bot struct {
	id      int
	low     int
	high    int
	execute func()
}

func (b *bot) receive(chip int) {
	if chip > b.high {
		b.low = b.high
		b.high = chip
	} else {
		b.low = chip
	}
	if b.low > 0 && b.high > 0 {
		fmt.Printf("bot %d sorted %d and %d\n", b.id, b.low, b.high)
		b.execute()
	}
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	bots := map[int]*bot{}
	outputs := map[int]int{}
	gives := make([]map[int]int, 0, 100)

	for scanner.Scan() {
		input := scanner.Text()

		if strings.HasPrefix(input, "value ") {
			botID, chip := 0, 0
			fmt.Sscanf(input, scanVal, &chip, &botID)
			gives = append(gives, map[int]int{chip: botID})
		} else if strings.HasPrefix(input, "bot ") {
			botID, lowTarget, highTarget := 0, 0, 0
			lowType, highType := "", ""
			fmt.Sscanf(input, scanBot, &botID, &lowType, &lowTarget, &highType, &highTarget)
			b := &bot{id: botID}
			b.execute = func() {
				b := bots[botID]

				switch lowType {
				case "bot":
					bots[lowTarget].receive(b.low)
				case "output":
					outputs[lowTarget] = b.low
				}

				switch highType {
				case "bot":
					bots[highTarget].receive(b.high)
				case "output":
					outputs[highTarget] = b.high
				}
			}
			bots[b.id] = b
		}
	}

	for _, g := range gives {
		for c, b := range g {
			bots[b].receive(c)
		}
	}

	fmt.Println(outputs)
	fmt.Println(outputs[0] * outputs[1] * outputs[2])
}
