package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	input := bufio.NewReader(r)

	cstart := byte('(')
	cend := byte(')')
	incommand := false

	sum := 0
	command := bytes.Buffer{}
	for {
		b, err := input.ReadByte()
		if err != nil {
			break
		}
		if !unicode.IsSpace(rune(b)) {
			if incommand {
				if b == cend {
					incommand = false
				} else {
					command.WriteByte(b)
				}
			} else if command.Len() > 0 {
				raw := strings.Split(command.String(), "x")
				num, _ := strconv.Atoi(raw[0])
				repeat, _ := strconv.Atoi(raw[1])
				sum += (num * repeat)

				input.UnreadByte()
				part := make([]byte, num)
				n, err := input.Read(part)
				if n < num {
					for i := n; n > 0 && err == nil; {
						n, err = input.Read(part[i:])
						i += n
					}
				}
				command.Reset()
			} else {
				if b == cstart {
					incommand = true
				} else {
					sum++
				}
			}
		}
	}

	fmt.Println(sum)
}
