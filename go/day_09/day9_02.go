package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

const cstart = byte('(')
const cend = byte(')')

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	input := bufio.NewReader(r)
	fmt.Println(count(input))
}

func count(input *bufio.Reader) int {
	return processCount(input, -1)
}

func processCount(input *bufio.Reader, n int) int {
	incommand := false
	sum := 0
	command := bytes.Buffer{}
	read := 0
	for n < 0 || read < n {
		b, err := input.ReadByte()
		read++
		if err != nil {
			break
		}
		if !unicode.IsSpace(rune(b)) {
			if incommand {
				if b == cend {
					incommand = false
				} else {
					command.WriteByte(b)
				}
			} else if command.Len() > 0 {
				raw := strings.Split(command.String(), "x")
				command.Reset()

				num, _ := strconv.Atoi(raw[0])
				repeat, _ := strconv.Atoi(raw[1])

				input.UnreadByte()
				read += (num - 1)
				add := processCount(input, num)
				sum += add * repeat
			} else {
				if b == cstart {
					incommand = true
				} else {
					sum++
				}
			}
		}
	}

	return sum
}
