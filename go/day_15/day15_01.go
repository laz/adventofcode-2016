package main

import (
	"bufio"
	"fmt"
	"os"
)

const scan = "Disc #%d has %d positions; at time=0, it is at position %d."

type disc struct {
	number    int
	positions int
	zeroSlot  int
	start     int
}

func (d *disc) String() string {
	return fmt.Sprintf("{%d %d %d %d}", d.number, d.positions, d.zeroSlot, d.start)
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	discs := make([]*disc, 0, 20)

	for scanner.Scan() {
		input := scanner.Text()

		d, l, s := 0, 0, 0
		fmt.Sscanf(input, scan, &d, &l, &s)
		slot := (l - (s % l)) % l
		disc := &disc{d, l, slot, s}
		discs = append(discs, disc)
	}

	found := false
	t := 0
	for !found {
		tests := 0
		for _, disc := range discs {
			if (t+disc.number-disc.zeroSlot)%disc.positions == 0 {
				tests++
			}
		}
		found = tests == len(discs)
		if !found {
			t++
		}
	}

	fmt.Println(t)
}
