package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"unicode"
)

type node struct {
	x     int
	y     int
	open  bool
	cost  int
	print rune
}

func (n *node) String() string {
	return fmt.Sprintf("{%d %d %t %d %c}", n.x, n.y, n.open, n.cost, n.print)
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	definition := make([]string, 0, 50)
	for scanner.Scan() {
		definition = append(definition, scanner.Text())
	}

	points := []*node{}
	maze := make([][]*node, len(definition))
	for y, s := range definition {
		maze[y] = make([]*node, len(s))
		for x, r := range s {
			maze[y][x] = &node{x: x, y: y, open: r != '#', print: r}
			if unicode.IsDigit(r) {
				points = append(points, &node{x: x, y: y, open: r != '#', print: r})
				if r == '0' && len(points) > 1 {
					points = append(points[len(points)-1:], points[0:len(points)-1]...)
				}
			}
		}
	}

	distances := map[string]int{}
	for i := 0; i < len(points); i++ {
		start := points[i]
		for j := i + 1; j < len(points); j++ {
			end := points[j]
			visited := map[string]*node{}
			visited[makeKey(start)] = start
			cost := 0
			domain := []*node{start}
			for 0 < len(domain) {
				next := []*node{}
				for _, n := range domain {
					if *n == *end {
						distances[makeKey(start)+":"+makeKey(end)] = cost
						distances[makeKey(end)+":"+makeKey(start)] = cost
						domain = []*node{}
						break
					}
					tests := getAdjacents(n, maze)
					for _, t := range tests {
						//t.cost = cost
						if take(t, visited) {
							next = append(next, t)
						}
						visited[makeKey(t)] = t
					}
				}
				domain = next
				cost++
			}
		}
	}

	permutations := perm(points[1:], 0)
	least := math.MaxInt64
	path := []*node{}
	for _, p := range permutations {
		sum := distances[makeKey(points[0])+":"+makeKey(p[0])]
		for j := 0; j < len(p)-1; j++ {
			sum += distances[makeKey(p[j])+":"+makeKey(p[j+1])]
		}
		if sum < least {
			least = sum
			path = p
		}
	}

	fmt.Println(least, path)
}

func makeKey(n *node) string {
	return fmt.Sprintf("%d:%d", n.x, n.y)
}

func take(n *node, v map[string]*node) bool {
	if n.open {
		t, ok := v[makeKey(n)]
		if !ok || n.cost < t.cost {
			return true
		}
	}
	return false
}

func getAdjacents(n *node, d [][]*node) []*node {
	a := []*node{}
	if n.y-1 >= 0 {
		a = append(a, d[n.y-1][n.x])
	}
	if n.x+1 < len(d[0]) {
		a = append(a, d[n.y][n.x+1])
	}
	if n.y+1 < len(d) {
		a = append(a, d[n.y+1][n.x])
	}
	if n.x-1 >= 0 {
		a = append(a, d[n.y][n.x-1])
	}
	return a
}

func printMaze(maze [][]*node) {
	fmt.Print("  ")
	for x := 0; x < len(maze[0]); x++ {
		fmt.Printf("%2d ", x)
	}
	fmt.Println()

	for y := 0; y < len(maze); y++ {
		fmt.Printf("%2d", y)
		for x := 0; x < len(maze[y]); x++ {
			fmt.Print(" ", string(maze[y][x].print), " ")
		}
		fmt.Println()
	}
}

func perm(nodes []*node, i int) [][]*node {
	res := [][]*node{}
	if i == len(nodes) {
		res = append(res, nodes)
	} else {
		n := make([]*node, len(nodes), (cap(nodes)+1)*2)
		copy(n, nodes)
		for j := i; j < len(n); j++ {
			n[i], n[j] = n[j], n[i]
			res = append(res, perm(n, i+1)...)
			n[i], n[j] = n[j], n[i]
		}
	}
	return res
}
