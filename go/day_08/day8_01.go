package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	num_cols := 50
	num_rows := 6
	screen := make([][]bool, num_rows)
	for i := 0; i < len(screen); i++ {
		screen[i] = make([]bool, num_cols)
	}
	for scanner.Scan() {
		command := strings.Split(scanner.Text(), " ")

		switch command[0] {
		case "rect":
			args := strings.Split(command[1], "x")
			cols, _ := strconv.Atoi(args[0])
			rows, _ := strconv.Atoi(args[1])

			for r := 0; r < rows; r++ {
				for c := 0; c < cols; c++ {
					if r < num_rows && c < num_cols {
						screen[r][c] = true
					}
				}
			}
		case "rotate":
			args := strings.Split(command[2], "=")
			dir := args[0]
			loc, _ := strconv.Atoi(args[1])
			dist, _ := strconv.Atoi(command[4])

			switch dir {
			case "x":
				col := make([]bool, num_rows)
				for i := 0; i < num_rows; i++ {
					t := (i + dist) % num_rows
					col[t] = screen[i][loc]
				}
				for i := 0; i < num_rows; i++ {
					screen[i][loc] = col[i]
				}
			case "y":
				row := make([]bool, num_cols)
				for i := 0; i < num_cols; i++ {
					t := (i + dist) % num_cols
					row[t] = screen[loc][i]
				}
				for i := 0; i < num_cols; i++ {
					screen[loc][i] = row[i]
				}
			}
		}
	}
	sum := 0
	for _, row := range screen {
		for _, pixel := range row {
			if pixel {
				sum++
				fmt.Print("# ")
			} else {
				fmt.Print(". ")
			}
		}
		fmt.Println()
	}

	fmt.Println(sum)
}
