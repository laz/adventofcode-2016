package main

import (
	"crypto/md5"
	"fmt"
	"os"
	"strconv"
)

func main() {
	salt := "ahsbgdzn"
	if len(os.Args) > 1 {
		salt = os.Args[1]
	}

	size := 64
	count := 0
	cache := map[int]string{}
	keys := make([]int, 0, size)

	for len(keys) < size {
		sum, ok := cache[count]
		if !ok {
			test := salt + strconv.Itoa(count)
			sum = fmt.Sprintf("%x", md5.Sum([]byte(test)))
		}

		if pass, char := testKey(sum, 3, 0); pass {
			for more := 1; more <= 1000; more++ {
				sum, ok := cache[count+more]
				if !ok {
					test := salt + strconv.Itoa(count+more)
					sum = fmt.Sprintf("%x", md5.Sum([]byte(test)))
					cache[count+more] = sum
				}

				if ok, _ := testKey(sum, 5, char); ok {
					keys = append(keys, count)
				}
			}
		}
		count++
	}

	fmt.Println(keys)
}

func testKey(s string, l int, c byte) (bool, byte) {
	for i := 0; i < len(s)-(l-1); i++ {
		count := 0
		if c > 0 && s[i] != c {
			continue
		}
		for j := 1; j < l; j++ {
			if s[i] == s[i+j] {
				count++
			}
		}
		if count == (l - 1) {
			return true, s[i]
		}
	}
	return false, 0
}
