package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	turn := map[string]int{"L": -1, "R": 1}
	cur := []int{0, 0, 0}
	move := [][]int{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}

	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		input := strings.TrimSuffix(scanner.Text(), ",")
		steps, err := strconv.Atoi(input[1:])
		if err != nil {
			fmt.Println("skipping invalid input: ", input)
		} else {
			cur[0] = (cur[0] + turn[input[:1]] + 4) % 4
			if cur[0]%2 == 1 {
				cur[1] = cur[1] + (move[cur[0]][0] * steps)
			} else {
				cur[2] = cur[2] + (move[cur[0]][1] * steps)
			}
		}
	}
	absx := cur[1]
	if absx < 0 {
		absx = -absx
	}
	absy := cur[2]
	if absy < 0 {
		absy = -absy
	}
	fmt.Printf("%d blocks\n", absx+absy)
}
