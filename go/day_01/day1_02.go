package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type point struct {
	x int
	y int
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	turn := map[string]int{"L": -1, "R": 1}
	direction := 0
	location := point{0, 0}
	move := []point{point{0, 1}, point{1, 0}, point{0, -1}, point{-1, 0}}
	visited := map[string]bool{}
	found := false

	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		input := strings.TrimSuffix(scanner.Text(), ",")
		steps, err := strconv.Atoi(input[1:])
		if err != nil {
			fmt.Println("skipping invalid input: ", input)
		} else {

			direction = (direction + turn[input[:1]] + 4) % 4
			for step := 1; step <= steps; step++ {
				if direction%2 == 1 {
					location.x = location.x + move[direction].x
				} else {
					location.y = location.y + move[direction].y
				}
				key := fmt.Sprintf("%d:%d", location.x, location.y)
				if visited[key] {
					found = true
					break
				}
				visited[key] = true
			}
			if found {
				break
			}
		}
	}
	if !found {
		fmt.Printf("not found")
	} else {
		absx := location.x
		if absx < 0 {
			absx = -absx
		}
		absy := location.y
		if absy < 0 {
			absy = -absy
		}
		fmt.Printf("%d blocks\n", absx+absy)
	}
}
