package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

type point struct {
	x int
	y int
}

type line struct {
	a point
	b point
}

func (l line) horizontal() bool {
	return l.a.y == l.b.y
}

func (l line) vertical() bool {
	return l.a.x == l.b.x
}

func (l line) intersect(t line) (point, bool) {
	p := point{0, 0}
	b := false
	if l.horizontal() && t.vertical() {
		b = l.a.x < t.a.x && l.b.x > t.a.x && t.a.y < l.a.y && t.b.y > l.a.y
		p = point{t.a.x, l.a.y}
	} else if l.vertical() && t.horizontal() {
		b = l.a.y < t.a.y && l.b.y > t.a.y && t.a.x < l.a.x && t.b.x > l.a.x
		p = point{l.a.x, t.a.y}
	}
	return p, b
}

type horizontal []line

func (h horizontal) Len() int           { return len(h) }
func (h horizontal) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }
func (h horizontal) Less(i, j int) bool { return h[i].a.y < h[j].a.y }

type vertical []line

func (v vertical) Len() int           { return len(v) }
func (v vertical) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v vertical) Less(i, j int) bool { return v[i].a.x < v[j].a.x }

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	turn := map[string]int{"L": -1, "R": 1}
	direction := 0
	location := point{0, 0}
	move := []point{point{0, 1}, point{1, 0}, point{0, -1}, point{-1, 0}}
	hlines := horizontal(make([]line, 0, 1000))
	vlines := vertical(make([]line, 0, 1000))

	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	final := point{0, 0}
	found := false
	for scanner.Scan() {
		input := strings.TrimSuffix(scanner.Text(), ",")
		steps, err := strconv.Atoi(input[1:])
		if err != nil {
			fmt.Println("skipping invalid input: ", input)
		} else {
			prev := location

			direction = (direction + turn[input[:1]] + 4) % 4
			location.x = location.x + (move[direction].x * steps)
			location.y = location.y + (move[direction].y * steps)

			l := line{prev, location}
			if direction > 1 {
				l = line{location, prev}
			}
			if l.vertical() {
				vlines = append(vlines, l)
				for _, s := range hlines {
					final, found = l.intersect(s)
					if found {
						fmt.Printf("FOUND going vertical %v %v %v\n", l, s, hlines)
						break
					}
				}
				sort.Sort(vlines)
			} else {
				hlines = append(hlines, l)
				for _, s := range vlines {
					final, found = l.intersect(s)
					if found {
						fmt.Printf("FOUND going horizontal %v %v %v\n", l, s, vlines)
						break
					}
				}
				sort.Sort(hlines)
			}
		}
	}
	absx := final.x
	if absx < 0 {
		absx = -absx
	}
	absy := final.y
	if absy < 0 {
		absy = -absy
	}
	fmt.Printf("%d blocks\n", absx+absy)
}
