package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"
)

func main() {
	p := "abcdefgh"
	if len(os.Args) > 1 {
		p = os.Args[1]
	}

	r := os.Stdin
	if len(os.Args) > 2 {
		if f, err := os.Open(os.Args[2]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		inst := strings.Split(scanner.Text(), " ")
		args := inst[1:]
		switch inst[0] {

		case "swap":
			i, j := 0, 0
			if args[0] == "position" {
				i, _ = strconv.Atoi(args[1])
			} else {
				i = strings.Index(p, args[1])
			}
			if args[3] == "position" {
				j, _ = strconv.Atoi(args[4])
			} else {
				j = strings.Index(p, args[4])
			}
			if j < i {
				i, j = j, i
			}
			p = p[:i] + string(p[j]) + p[i+1:j] + string(p[i]) + p[j+1:]

		case "reverse":
			i, _ := strconv.Atoi(args[1])
			j, _ := strconv.Atoi(args[3])
			if j < i {
				i, j = j, i
			}
			for j+1-i > 1 {
				p = p[:i] + string(p[j]) + p[i+1:j] + string(p[i]) + p[j+1:]
				i++
				j--
			}

		case "rotate":
			i := 0
			if args[0] == "based" {
				i = strings.Index(p, args[5])
				if i > 3 {
					i++
				}
				i++
			} else {
				i, _ = strconv.Atoi(args[1])
			}
			l := utf8.RuneCountInString(p)
			t := make([]rune, l)
			if args[0] == "left" {
				i = -i
			}
			for j, r := range p {
				t[(j+i+l)%l] = r
			}
			p = string(t)

		case "move":
			i, _ := strconv.Atoi(args[1])
			j, _ := strconv.Atoi(args[4])
			if j > i {
				p = p[:i] + p[i+1:j+1] + string(p[i]) + p[j+1:]
			} else {
				p = p[:j] + string(p[i]) + p[j:i] + p[i+1:]
			}
		}
	}

	fmt.Println(p)
}
