package main

import (
	"fmt"
)

type node struct {
	x     int
	y     int
	open  bool
	cost  int
	print rune
}

func (n *node) String() string {
	return fmt.Sprintf("{%d %d %t %d %c}", n.x, n.y, n.open, n.cost, n.print)
}

func main() {
	start := &node{x: 1, y: 1, open: true, print: 'S'}
	end := &node{x: 31, y: 39, open: true, cost: 0, print: 'X'}
	maxy := 43
	maxx := 36
	input := 1358

	maze := make([][]*node, maxy)
	for y := 0; y < len(maze); y++ {
		maze[y] = make([]*node, maxx)
		for x := 0; x < len(maze[y]); x++ {
			calc := (x * x) + (3 * x) + (2 * x * y) + y + (y * y) + input
			count := OnesCount(uint64(calc))
			if x == end.x && y == end.y {
				maze[y][x] = end
			} else if count&1 == 0 {
				maze[y][x] = &node{x: x, y: y, open: true, print: '.'}
			} else {
				maze[y][x] = &node{x: x, y: y, open: false, print: '#'}
			}
		}
	}

	visited := map[string]*node{}
	visited[makeKey(end)] = end

	cost := 0
	domain := []*node{end}
	for 0 < len(domain) {
		next := []*node{}
		for _, n := range domain {
			if n == start {
				domain = []*node{}
				break
			}
			tests := getAdjacents(n, maze)
			for _, t := range tests {
				t.cost = cost
				if take(t, visited) {
					maze[t.y][t.x].print = 'O'
					next = append(next, t)
				}
				visited[makeKey(t)] = t
			}
		}
		domain = next
		cost++
	}

	fmt.Println(getAdjacents(start, maze))
}

func makeKey(n *node) string {
	return fmt.Sprintf("%d:%d", n.x, n.y)
}

func take(n *node, v map[string]*node) bool {
	if n.open {
		t, ok := v[makeKey(n)]
		if !ok || n.cost < t.cost {
			return true
		}
	}
	return false
}

func getAdjacents(n *node, d [][]*node) []*node {
	a := []*node{}
	if n.y-1 >= 0 {
		a = append(a, d[n.y-1][n.x])
	}
	if n.x+1 < len(d[0]) {
		a = append(a, d[n.y][n.x+1])
	}
	if n.y+1 < len(d) {
		a = append(a, d[n.y+1][n.x])
	}
	if n.x-1 >= 0 {
		a = append(a, d[n.y][n.x-1])
	}
	return a
}

func PrintMaze(maze [][]*node) {
	fmt.Print("  ")
	for x := 0; x < len(maze[0]); x++ {
		fmt.Printf("%2d ", x)
	}
	fmt.Println()

	for y := 0; y < len(maze); y++ {
		fmt.Printf("%2d", y)
		for x := 0; x < len(maze[y]); x++ {
			fmt.Print(" ", string(maze[y][x].print), " ")
		}
		fmt.Println()
	}
}

const m0 = 0x5555555555555555 // 01010101 ...
const m1 = 0x3333333333333333 // 00110011 ...
const m2 = 0x0f0f0f0f0f0f0f0f // 00001111 ...

func OnesCount(x uint64) int {
	const m = 1<<64 - 1
	x = x>>1&(m0&m) + x&(m0&m)
	x = x>>2&(m1&m) + x&(m1&m)
	x = (x>>4 + x) & (m2 & m)
	x += x >> 8
	x += x >> 16
	x += x >> 32
	return int(x) & (1<<7 - 1)
}
